# CI Templates

A collection of useful GitLab CI/CD templates.

## Usage

Simply [`include`][1] the relevant template(s) in your project's `.gitlab-ci.yml` file:

```yaml
# .gitlab-ci.yml
---
include:
  # Replace <REF> with the branch or tag name and 
  # <TEMPLATE> with the template file name. 
  - remote: 'https://gitlab.com/mwalto7/ci-templates/-/raw/<REF>/<TEMPLATE>.gitlab-ci.yml' 
```

[1]: https://docs.gitlab.com/ee/ci/yaml/#include
